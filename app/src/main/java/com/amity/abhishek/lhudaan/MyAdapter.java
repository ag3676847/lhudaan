package com.amity.abhishek.lhudaan;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import androidx.annotation.NonNull;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;

import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;


public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {
    private ArrayList<String> Names;
    ArrayList<String> blood;
    private Context context;
    MyAdapter(Context context, ArrayList<String> Name, ArrayList<String> blood) {
        Names = Name;
        this.context=context;
        this.blood=blood;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_item, viewGroup, false);
        return new MyAdapter.MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyAdapter.MyViewHolder myViewHolder, int i) {
        myViewHolder.Name.setText(Names.get(i));
        myViewHolder.setClickListener(new ItemClickListener() {
            @Override
            public void onClick(View view, int position) {
                Intent intent= new Intent(context, ReceiverActivity.class);
                intent.putExtra("name", Names.get(position));
                intent.putExtra("blood", blood.get(position));
                context.startActivity(intent);
                ((Activity)context).finish();

            }
        });
    }

    @Override
    public int getItemCount() {
        return Names.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView Name;
        CardView c;
        private ItemClickListener clickListener;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            c = itemView.findViewById(R.id.card_view);
            c.setOnClickListener(this);
            Name = itemView.findViewById(R.id.name_textview);

        }

        private void setClickListener(ItemClickListener itemClickListener){
            this.clickListener=itemClickListener;
        }

        @Override
        public void onClick(View v) {
            clickListener.onClick(v,getLayoutPosition());
        }
    }
}