package com.amity.abhishek.lhudaan;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;

public class welcome_activity extends AppCompatActivity {

    FirebaseAuth firebaseAuth= FirebaseAuth.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        if(firebaseAuth.getCurrentUser()!=null){
            if((firebaseAuth.getCurrentUser().getUid()+"").equals("SR9f0qgK2JfvUpPaqNuqAhh8JVk1")){
                startActivity(new Intent(this, adminActivity.class));
                finish();
            }else {
                startActivity(new Intent(welcome_activity.this, MainActivity.class));
                finish();
            }
        }else{
            Button s=findViewById(R.id.signup);
            s.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent= new Intent(welcome_activity.this, SignupActivity.class);
                    startActivity(intent);
                    finish();
                }
            });

            Button l=findViewById(R.id.signin);
            l.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i= new Intent(welcome_activity.this,SignInActivity.class);
                    startActivity(i);
                    finish();
                }
            });
        }



    }
}
