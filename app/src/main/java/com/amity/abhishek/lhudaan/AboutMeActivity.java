package com.amity.abhishek.lhudaan;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.Html;
import android.text.method.ScrollingMovementMethod;
import android.widget.TextView;

public class AboutMeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_me);

        String myHtmlFormattedText = "•Every year our nation requires about 5 Crore units of blood, out of which only a meager 2.5 Crore units of blood are available. \n" +
                "" +
                "•The gift of blood is the gift of life. There is no substitute for human blood. \n" +
                "" +
                "•Every two seconds someone needs blood. \n" +
                "" +
                "•More than 38,000 blood donations are needed every day. \n" +
                "" +
                "•A total of 30 million blood components are transfused each year. \n" +
                "" +
                "•The average red blood cell transfusion is approximately 3 pints. \n" +

                "•The blood type most often requested by hospitals is Type O. \n" +
                " \n" +
                "•Sickle cell patients can require frequent blood transfusions throughout their lives. \n" +

                "•More than 1 million new people are diagnosed with cancer each year. Many of them will    need blood, sometimes daily, during their chemotherapy treatment. \n" +

                "A single car accident victim can require as many as 100 units of blood\" \n" +

                "Blood cannot be manufactured – it can only come from generous donors. \n" +

                "Type O-negative blood (red cells) can be transfused to patients of all blood types. It is always in great demand and often in short supply. \n" +
                " \n" +
                "Type AB-positive plasma can be transfused to patients of all other blood types. AB plasma is also usually in short supply. Facts about the blood donation process\" \n" +

                "Donating blood is a safe process. A sterile needle is used only once for each donor and then    discarded. \n" +
                " \n" +
                "Blood donation is a simple four-step process: registration, medical history and mini-physical, donation and refreshments. \n" +
                " \n" +
                "Every blood donor is given a mini-physical, checking the donors temperature, blood pressure, pulse and hemoglobin to ensure it is safe for the donor to give blood. \n" +
                " \n" +
                "The actual blood donation typically takes less than 10-12 minutes. The entire process, from the time you arrive to the time you leave, takes about an hour and 15 min. \n" +
                " \n" +
                "The average adult has about 10 units of blood in his body. Roughly 1 unit is given during a donation.\n" +
                " \n" +
                "A healthy donor may donate red blood cells every 56 days, or double red cells every 112 days. \n" +
                " \n" +
                "A healthy donor may donate platelets as few as 7 days apart, but a maximum of 24 times a year. \n" +
                " \n" +
                "All donated blood is tested for HIV, hepatitis B and C, syphilis and other infectious diseases before it can be transfused to patients.\" \n" +
                "\n" +
                "There are four types of transfusable products that can be derived from blood: red cells, platelets, plasma and cryoprecipitate. Typically, two or three of these are produced from a unit of donated whole blood – hence each donation can help save up to three lives. \n" +
                " \n" +
                "Donors can give either whole blood or specific blood components only. The process of donating specific blood components – red cells, plasma or platelets – is called apheresis. \n" +

                "One transfusion dose of platelets can be obtained through one apheresis donation of platelets or by combining the platelets derived from five whole blood donations. \n" +
                " \n" +
                "Donated platelets must be used within five days of collection. \n" +

                "Healthy bone marrow makes a constant supply of red cells, plasma and platelets. The body will replenish the elements given during a blood donation – some in a matter of hours and others in a matter of weeks.\"\n" +
                "\n" +
                "The number one reason donors say they give blood is because they want to help others. \n" +

                "Two most common reasons cited by people who dont give blood are: Never thought about it and I dont like needles. \n" +

                "One donation can help save the lives of up to three people. \n" +
                "If you began donating blood at age 18 and donated every 90 days until you reached 60, you would have donated 30 gallons of blood, potentially helping save more than 500 lives! \n" +

                " Only 7 percent of people in India have O-negative blood type. O-negative blood type donors are universal donors as their blood can be given to people of all blood types. \n" +

                "Type O-negative blood is needed in emergencies before the patients blood type is known and with newborns who need blood. \n" +

                "Thirty-five percent of people have Type O (positive or negative) blood.  \n" +

                "0.4 percent of people have AB-blood type. AB-type blood donors are universal donors of plasma, which is often used in emergencies, for newborns and for patients requiring massive transfusions.\n\n" +

                "*************\n\n" +

                "This app is developed by Ashish";

        TextView tv = (TextView) findViewById(R.id.textView);
        tv.setText(myHtmlFormattedText);
        tv.setMovementMethod(new ScrollingMovementMethod());

    }
}
