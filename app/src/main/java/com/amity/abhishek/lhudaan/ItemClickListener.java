package com.amity.abhishek.lhudaan;

import android.view.View;

public interface ItemClickListener {
    void onClick(View view, int position);
}
