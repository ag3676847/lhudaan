package com.amity.abhishek.lhudaan;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

public class details_activity extends AppCompatActivity {
    // Access a Cloud Firestore instance from your Activity
    FirebaseFirestore db = FirebaseFirestore.getInstance();
     private EditText name,enrollno,bloodgroup,address,phoneno,campus,nationality,session;

     String donor_receiver;
     String disease;

    private FirebaseAuth mAuth;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.details_activity);

        Toast.makeText(this, "Reached", Toast.LENGTH_LONG).show();


        name=findViewById(R.id.name);
        enrollno=findViewById(R.id.enrollno);
        bloodgroup=findViewById(R.id.blood_group);
        address=findViewById(R.id.Address);
        phoneno=findViewById(R.id.Phone);
        campus=findViewById(R.id.campus);
        nationality=findViewById(R.id.nationality);
        session=findViewById(R.id.session);






        Button save_buttton= findViewById(R.id.save_btn);
        save_buttton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent=getIntent();

                //Toast.makeText(details_activity.this,bloodgroup.getText().toString(), Toast.LENGTH_SHORT).show();
                if(donor_receiver.equals("donor")) {
                    if (disease.equals("No")) {
                        signUp(intent.getStringExtra("email"), intent.getStringExtra("passwd"));
                        uploadToFirebase();
                        Intent i = new Intent(details_activity.this, MainActivity.class);
                        startActivity(i);
                        finish();
                    } else {
                       // Toast.makeText(details_activity.this, "Not Eligible", Toast.LENGTH_SHORT).show();
                        AlertDialog.Builder alert=new AlertDialog.Builder(details_activity.this);
                        alert.setMessage("Sorry, you are not eligible.");
                        DialogInterface.OnClickListener dialogListener = new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                                startActivity(new Intent(details_activity.this, welcome_activity.class));
                            }
                        };
                        alert.setPositiveButton("Ok", dialogListener);
                        alert.show();

                    }
                }else{
                    signUp(intent.getStringExtra("email"), intent.getStringExtra("passwd"));
                    uploadToFirebase();
                    Intent i = new Intent(details_activity.this, MainActivity.class);
                    startActivity(i);
                    finish();
                }





            }
        });
    }

    private void signUp(String email, String password) {
        FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
        firebaseAuth.createUserWithEmailAndPassword(email, password)
            .addOnCompleteListener(details_activity.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {

                        // If sign in fails, display a message to the user. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in user can be handled in the listener.
                        if (!task.isSuccessful()) {
                            Toast.makeText(details_activity.this, "Authentication failed." + task.getException(),
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
            });
    }

    private void uploadToFirebase() {
        Map< String, String > userdetails = new HashMap< >();
        userdetails.put("name", name.getText().toString());
        userdetails.put("type", donor_receiver);
        userdetails.put("bloodGroup", bloodgroup.getText().toString());
        userdetails.put("enrollmentno",enrollno.getText().toString());
        userdetails.put("address", address.getText().toString());
        userdetails.put("phoneno", phoneno.getText().toString());
        userdetails.put("campus", campus.getText().toString());
        userdetails.put("nationality", nationality.getText().toString());
        userdetails.put("session", session.getText().toString());

                /*user_type_details.put(bloodgroup.getText().toString(), userdetails);
                db.collection(donor_receiver).add(user_type_details).addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {*/
        db.collection(bloodgroup.getText().toString().toUpperCase()).document(name.getText().toString()).set(userdetails)

                .addOnSuccessListener(new OnSuccessListener < Void > () {

                    @Override

                    public void onSuccess(Void aVoid) {
                        Toast.makeText(details_activity.this, "user registered ", Toast.LENGTH_SHORT).show();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        String TAG = "Firebase";
                        Log.w(TAG, "Error adding document", e);
                    }
                });
            }
    public void onRadioButtonClicked(View view){
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.donor:
                if (checked)
                    donor_receiver="donor";
                    break;
            case R.id.reciever:
                if (checked)
                    donor_receiver="receiver";
                    break;
        }

    }
    public void onRadiodiseaseButtonClicked(View view){
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.yes:
                if (checked)
                    disease="Yes";
                break;
            case R.id.no:
                if (checked)
                    disease="No";
                break;
        }

    }
}
