package com.amity.abhishek.lhudaan;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.Objects;

public class ReceiverActivity extends AppCompatActivity {

    FirebaseFirestore db = FirebaseFirestore.getInstance();
    TextView donordetails;
    String name,blood, phone;
    Button ask;
    FirebaseAuth auth = FirebaseAuth.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receiver);

        donordetails = findViewById(R.id.donordetails);

        fetch();

        ask = findViewById(R.id.ask);

        ask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                try{
                    if (ContextCompat.checkSelfPermission(ReceiverActivity.this, Manifest.permission.SEND_SMS)
                            != PackageManager.PERMISSION_GRANTED) {

                        ActivityCompat.requestPermissions(ReceiverActivity.this,
                                new String[]{Manifest.permission.SEND_SMS}, 1);
                    }else{
                        sendSMS();
                    }



                }catch (Exception e){
                    Toast.makeText(ReceiverActivity.this, ""+e, Toast.LENGTH_LONG).show();
                }

            }
        });

    }

    private void sendSMS(){
        String message = "Hi, "+name+" has requested to donate blood using the AmityBeCure app. " +
                "Contact the person at: "+phone;
        SmsManager smsManager = SmsManager.getDefault();
        smsManager.sendTextMessage(phone, null, message, null, null);
        AlertDialog.Builder alert = new AlertDialog.Builder(ReceiverActivity.this);
        alert.setMessage("Message Sent!");
        alert.setPositiveButton("Ok", null);
        alert.show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    sendSMS();
                } else {
                    AlertDialog.Builder alert = new AlertDialog.Builder(ReceiverActivity.this);
                    alert.setMessage("You can't send SMS without giving permission!");
                    alert.setPositiveButton("Ok", null);
                    alert.show();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request.
        }
    }


    private void fetch() {
        Intent intent = getIntent();
         name = intent.getStringExtra("name");
         blood = intent.getStringExtra("blood");
        DocumentReference user = db.collection(blood).document(name);
        user.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot doc = task.getResult();
                    phone= Objects.requireNonNull(doc).get("phoneno")+"";
                    String fields = "" + "Name: " + doc.get("name") +
                            "\nBlood_Type: " + doc.get("bloodGroup") +
                            "\nAddress: " + doc.get("address") +
                            "\nCampus: " + doc.get("campus") +
                            "\nPhone: " + phone +
                            "\nEnrollment No.: " + doc.get("enrollmentno") +
                            "\nSession: " + doc.get("session") +
                            "\nNationality: " + doc.get("nationality") +
                            "\nType: " + doc.get("type");
                    donordetails.setText(fields);
                    ask.setVisibility(View.VISIBLE);
                }

            }

        })

                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {

                    }

                });
    }

    @Override
    public void onBackPressed() {

        if((auth.getCurrentUser().getUid()+"").equals("SR9f0qgK2JfvUpPaqNuqAhh8JVk1")) {
            finish();
            startActivity(new Intent(this, adminActivity.class));
        }else {
            finish();
            startActivity(new Intent(this, MainActivity.class));
        }
    }
}
