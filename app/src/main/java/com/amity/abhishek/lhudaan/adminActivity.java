package com.amity.abhishek.lhudaan;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Objects;

public class adminActivity extends AppCompatActivity {

    String[] blood= {"A+","A-","B+","B-","O+","O-","AB+","AB-"};
    RecyclerView rc;
    MyAdapter adapter;
    FirebaseFirestore db = FirebaseFirestore.getInstance();
    boolean empty=true;
    ArrayList<String> Names= new ArrayList<>();
    ArrayList<String> bloodGroup=new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);

        rc=findViewById(R.id.my_rc_view);
        rc.setVisibility(View.GONE);

        for(int i=0; i<8;i++) {
            db.collection(blood[i])
                    .get()
                    .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                            if (task.isSuccessful()) {
                                for (QueryDocumentSnapshot document : Objects.requireNonNull(task.getResult())) {
                                    //Toast.makeText(MainActivity.this,document.getId()+"", Toast.LENGTH_LONG).show();
                                    Names.add(document.getId() + "");
                                    bloodGroup.add(document.get("bloodGroup").toString());
                                    empty = false;
                                    adapter.notifyDataSetChanged();
                                }
                            } else {
                                Toast.makeText(adminActivity.this, task.getException() + "", Toast.LENGTH_LONG).show();
                            }
                        }
                    });
        }
        if(empty){
            TextView textView= findViewById(R.id.No_donors);
            textView.setVisibility(View.VISIBLE);
        }
        rc.setVisibility(View.VISIBLE);
        adapter = new MyAdapter(adminActivity.this, Names, bloodGroup);
        rc.setLayoutManager(new LinearLayoutManager(adminActivity.this));
        rc.setAdapter(adapter);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_logout) {
            logout();
            return true;
        }
        else if(id == R.id.action_about_me){
            startActivity(new Intent(this, AboutMeActivity.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    private void logout() {
        FirebaseAuth.getInstance().signOut();
        finish();
        Intent intent= new Intent(this, welcome_activity.class);
        startActivity(intent);
    }
}
