package com.amity.abhishek.lhudaan;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.annotation.NonNull;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Objects;

public class MainActivity extends AppCompatActivity {
    FirebaseAuth mAuth = FirebaseAuth.getInstance();
    ArrayList<String> blood=new ArrayList<>();
    ArrayList<String> bloodGroup = new ArrayList<>();
    ArrayList<String> Names= new ArrayList<>();
    private RadioGroup b;
    Button Search;
    RecyclerView rc;
    MyAdapter adapter;
    boolean empty=true;
    final String TAG= "My_TAG";
    FirebaseFirestore db = FirebaseFirestore.getInstance();

    public void getUserID(){
        Intent intent=getIntent();
        String userid=intent.getStringExtra("User Id");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getUserID();
        Search=findViewById(R.id.search);
        b=findViewById(R.id.user_type);
        rc=findViewById(R.id.my_rc_view);
        rc.setVisibility(View.GONE);
        Search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b.setVisibility(View.GONE);
                Search.setVisibility(View.GONE);
                db.collection(blood.get(0))
                        .get()
                        .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                            @Override
                            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                if (task.isSuccessful()) {
                                    for (QueryDocumentSnapshot document : Objects.requireNonNull(task.getResult())) {
                                        //Toast.makeText(MainActivity.this,document.getId()+"", Toast.LENGTH_LONG).show();
                                        if(Objects.equals(document.getData().get("type"), "donor")){
                                            Names.add(document.getId()+"");
                                            bloodGroup.add(blood.get(0));
                                            empty=false;
                                        }
                                        adapter.notifyDataSetChanged();
                                    }
                                } else{
                                    Toast.makeText(MainActivity.this,task.getException()+"", Toast.LENGTH_LONG).show();
                                }
                            }
                        });
                if(empty){
                    TextView textView= findViewById(R.id.No_donors);
                    textView.setVisibility(View.VISIBLE);
                }
                    rc.setVisibility(View.VISIBLE);
                    adapter = new MyAdapter(MainActivity.this, Names, bloodGroup);
                    rc.setLayoutManager(new LinearLayoutManager(MainActivity.this));
                    rc.setAdapter(adapter);
            }
        });
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_logout) {
            logout();
            finish();
            return true;
        }
        else if(id == R.id.action_about_me){
            startActivity(new Intent(this, AboutMeActivity.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    private void logout() {
        FirebaseAuth.getInstance().signOut();
        Intent intent= new Intent(this, welcome_activity.class);
        startActivity(intent);
    }
    public void onbloodgroupClicked(View view){
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.A:
                if (checked)
                    blood.add("A+");
                break;
            case R.id.NegA:
                if (checked)
                    blood.add("A-");
                break;
            case R.id.B:
                if (checked)
                    blood.add("B+");
                break;
            case R.id.NegB:
                if (checked)
                    blood.add("B-");
                break;
            case R.id.O:
                if (checked)
                    blood.add("O+");
                break;
            case R.id.NegO:
                if (checked)
                    blood.add("O-");
                break;
            case R.id.AB:
                if (checked)
                    blood.add("AB+");
                break;
            case R.id.NegAB:
                if (checked)
                    blood.add("AB-");
                break;
        }

    }

    @Override
    public void onBackPressed() {
        if(rc.getVisibility()==View.VISIBLE){
            finish();
            startActivity(new Intent(this, MainActivity.class));
        }else{
            finish();
        }
    }
}

